#!/bin/bash
# Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

if [ -z "$1" ]
  then
    echo "./build.sh <reponame>"
    exit 1
fi

tagversion="$(cat version)"
docker build --rm -t $1/minifier:$tagversion .
  