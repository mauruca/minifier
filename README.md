# minifier

Minify you page, css and js without install node and html-minify

## Getting started

1. build the container
2. Prepare a minifier script to automate
```
#!/bin/sh
html-minifier \
--collapse-boolean-attributes --collapse-whitespace \
--decode-entities --html5 \
--collapse-boolean-attributes --collapse-inline-tag-whitespace \
--prevent-attributes-escaping --process-conditional-comments \
--remove-attribute-quotes --remove-comments --remove-empty-attributes \
--remove-optional-tags --remove-redundant-attributes --remove-script-type-attributes \
--remove-style-link-type-attributes --remove-tag-whitespace --sort-attributes --sort-class-name \
--trim-custom-fragments --use-short-doctype \
-o index_min.html index_notmin.html

html-minifier \
----minify-js \
-o js/site.min.js js_notmin/site.js
```

3. Run the container
```
docker run --rm -it --cpus=".10" -u $(id -u):$(id -g) -v $(pwd):/build yourepo/minifier:1.0 /bin/sh -c "cd /build;./_minify.sh"
```

## Minifier docs
For more minify options checkou the docs at [html-minifier](https://www.npmjs.com/package/html-minifier)